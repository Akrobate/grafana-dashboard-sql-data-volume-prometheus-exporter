# Grafana Dashboard Sql Data Volume Prometheus Exporter

Dashboards for sql data volume prometheus exporter.
This dashboard made to use the data of the Sql [volume data exporter](https://framagit.org/Akrobate/sql-data-volume-prometheus-exporter)

This metric is build to show the evolution on count lines of each table of a SQL database.
The exporter only needs some credential to database and a database name.

## Pannels

* Count variation pannel
* Total count per table pannel


### Count variation pannel

Variation pannel show the percent variation for the last 24 hours on each table

PromQL Query to get the variation count data
```
(
    (
        delta(database_db_name_table_line_count{name!=""}[24h])
    ) / (database_db_name_table_line_count{name!=""}
        - delta(database_db_name_table_line_count{name!=""}[24h]))
) 
```


### Total count per table pannel

Count variation pannel show the total rows per table in configured database.

PromQL Query to get the current counts
```
database_db_name_table_line_count{name!=""}
```


### [In progress] UpdatedAt count metric

Show per table the count of the updated lines. Workds on tables with technical field like "updated_at" that is updated on each modification of the row. The metric is a counter and counts each time a row is modified in database.


## Todo

* For the variation pannel, add variable of time ranges. 24h should be default value, but we should be able to customize it
